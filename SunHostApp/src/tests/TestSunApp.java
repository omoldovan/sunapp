
package tests;

import com.sun.spot.io.j2me.radiogram.*;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.microedition.io.*;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import net.java.dev.netbeansspot.DataManagement;
import net.java.dev.netbeansspot.SunSpotHostApplication;
import javax.microedition.io.Connector;


/**
 * <h1>Api connection Test class</h1>
 * This application is the host test module of SunApp API connection. 
 * This application hosts a simulated data collection from sensors wich wraps and
 * sends to API through DataManagement class.
 * 
 * @author Ovidiu Mircea Moldovan
 * @author Juan Perez
 * @version 1.0
* @since   2015-12-14 
 */
public class TestSunApp {

    // Broadcast port on which we listen for sensor samples
    private static final int HOST_PORT = 67;
    private static final int AGREGATOR_PORT=59;
    // Text area where the samples are displayed
    private JTextArea status;
    private DataManagement send;
    private String urlParameters;
    private URL api_url;
    private static final Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("proxylabdiatel.euitt.upm.es",
            3128));
    private String getUrl = "http://api.thingspeak.com/update?";
    
    private String postUrl="https://api.thingspeak.com/talkbacks/4532/commands/execute.json";
    private String postUrlParameters= "api_key=LXYMJZ9ZRCXHS16H";        

    private Thread testBroadcastTask;
    private RadiogramConnection rCon;
    private  RadiogramConnection rConSend;
    
    
    //ALARMS
    
    private static final String ALARMTEMP="ALARMA DE TEMPERATURA";
    private static final String ALARMLUM="ALARMA DE LUMINOSIDAD";
    private static final String ABRIRPUERTA="ABRIR PUERTA";
    private static final String CERRARPUERTA="CERRAR PUERTA";
    
    
    /**
     * Host mandatory method. Starts the host. Creates a JFrame where the status
     * will be displayed
     *
     */
    public void startApp() {
        JFrame fr = new JFrame("Send Data Host App");
        status = new JTextArea();
        JScrollPane sp = new JScrollPane(status);
        fr.add(sp);
        fr.setSize(360, 200);
        fr.validate();
        fr.setVisible(true);
        printStat("****Starting host app****", false);
        send = new DataManagement(status);
        try {
            api_url = new URL("http://api.thingspeak.com/update.json");
            
             
            testBroadcastTask(); 
        } catch (MalformedURLException ex) {
            printStat("Error " + Level.SEVERE + "  " + ex, true);
            Logger.getLogger(SunSpotHostApplication.class.getName()).log(Level.SEVERE, null, ex);
        } 
        catch (IOException ex) {
                Logger.getLogger(SunSpotHostApplication.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
 /**Host mandatory method.
     * Holds the program logic.
     * 
     * @throws Exception 
     */
    private void run() throws Exception {
        try {
            // Open up a server-side broadcast radiogram connection
        } catch (Exception e) {
            printStat("setUp caught " + e.getMessage(), true);
            throw e;
        }

        printStat("Listening...", false);

        // Main data collection loop
        while (true) {
            try {

                //dw.addData(time, val);
            } catch (Exception e) {
                printStat("Caught " + e + " while reading sensor samples.", true);

                throw e;
            }
        }
        
    }

    /**Main test function
     * 
     * @param option String that specifies if the request is GET or PUT
     */
    public void testAPI(String option) {
    // RadiogramConnection rCon;
        //    Radiogram dg;
        try {
            // Open up a server-side broadcast radiogram connection
            // to listen for sensor readings being sent by different SPOTs
            //        rCon = (RadiogramConnection) Connector.open("radiogram://:" + HOST_PORT);
            //        dg = (Radiogram)rCon.newDatagram(rCon.getMaximumLength());
            //        String ieee = IEEEAddress.toDottedHex(dg.getAddressAsLong());

            printStat("Received packet from SPOT: simulated", false);
            //TODO:send data
        } catch (Exception e) {
            printStat("setUp caught " + e.getMessage(), true);

        }

        printStat("Listening...", false);
        int i = 1;
        // Main data collection loop
        while (true) {

            i++;
            try {

                // Read  sensor sample received over the radio
                //        rCon.receive(dg);
                //DataWindow dw = findPlot(dg.getAddressAsLong());
                long time = 1000;      // read time of the reading
                int temp = i + 20;         // read the sensor value
                int light = i * 20;         // read the sensor value
                printStat("Temperature read is: " + temp
                        + " \n Lumminiscence read is: " + light, false);

                urlParameters = "api_key=IXTF61FOCJSLC3CX&field1="
                        + temp + "&field2=" + light;

                if (option.compareTo("GET") == 0) {
                    api_url = new URL(getUrl + urlParameters);

                    send.sendGet(api_url, proxy);
                    Thread.sleep(15000);
                } else {
                    api_url = new URL(postUrl);
                    send.sendPost(api_url, urlParameters, proxy);
                    Thread.sleep(15000);
                }

                //dw.addData(time, val);
            } catch (Exception e) {
                printStat("Caught " + e + " while reading sensor samples.", true);

            }
        }
    }

    
    private void testBroadcastCommands() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
     /**
     * 
     * @param rCon
     * @param dataG
     * @return 
     */
    private boolean broadcastDG( String dataG){
        try {
                          
            Datagram dg = rConSend.newDatagram(rConSend.getMaximumLength());  // 
            System.out.println("Mensaje a enviar: " + dataG);
         
            dg.writeUTF(dataG);
            rConSend.send(dg);
                
            return true;
        } catch (IOException ex) {
            Logger.getLogger(SunSpotHostApplication.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
    }
    
    /**
     * 
     * @param command
     * @return 
     */
    private String process_command(String command) {
       
        if (command.contains(ALARMTEMP)){
            return ALARMTEMP;
        }
        else if (command.contains(ALARMLUM)){
            return ALARMLUM;
        }
        else if (command.contains(ABRIRPUERTA)){
            return ABRIRPUERTA;
        }
        else if (command.contains(CERRARPUERTA)){
            return CERRARPUERTA;
        }
        else {
        printStat("The command recieved is not reconized",true);
        }
        
        return "";
        
        /*Cuando
la temperatura suba por encima de los 35°C
se 
añadirá
un comando de ALARMA DE 
TEMPERATURA a la cola de comandos.
2.
Cuando la luminosidad suba por encima de 
600 (
aprox. 1200 LX) se añadirá un comando de ALARMA DE 
LUMINOSIDAD a la cola de comandos.
3.
Cuando 
en 
la cuenta de Twitter creada en el punto 2 se publique un mensaje con el texto 
«ABRE LA 
PUERTA» se añadirá un comando de ABRIR PUERTA a la cola de comandos.
4.
Cuando en la cuenta de Twitter creada en el punto 2 se publique un mensaje con el texto «CIERRA LA 
P
UERTA» se añadirá un comando de CERRAR PUERTA a la cola de comandos.*/
        
        
    }
  
    
       /**
     * This method creates and starts a new Task or thread to monitor the
     * acceleration sensor. It reads the state each 500 * "acce_sleep_time"
     * milliseconds. If the new value is diferent from the first one then sets
     * all the leds to red.
     *
     * 
     */
    public void testBroadcastTask() {
        try {
            // start periodically reporting the acceleration
            // define the task - report position changed every 0.1 seconds
            rConSend = (RadiogramConnection) Connector.open("radiogram://broadcast:" + AGREGATOR_PORT);
        } catch (IOException ex) {
            Logger.getLogger(TestSunApp.class.getName()).log(Level.SEVERE, null, ex);
        }
      
        testBroadcastTask = new Thread( new Runnable(){
                

            public void run() {
                String loquesea;
                try {

                String command=send.getCommand(new URL(postUrl), postUrlParameters,proxy);
                printStat("Commando: " +command,false);
                
                while (command.compareTo("{}")!= 0 ){
                
                String a_message=process_command(command);
                printStat("Just recieved the command/alarm: "+a_message,false);
                //broadcast command
           
               if ( !a_message.isEmpty() && !broadcastDG( a_message))
                {
                    printStat("Error during command broadcast",true);
                }
                
                //check for new commands
                command=send.getCommand(new URL(postUrl), postUrlParameters, proxy);
                
                }

                } catch (IOException ex) {
                    ex.printStackTrace();
                    printStat("Error during command read",true);
                }
            }
            
        });

        testBroadcastTask.start();
    }

    /**
     * This method stops the thread started by this class.
     */
    public void broadcastStop() {
        testBroadcastTask.stop();
        //TaskManager.unschedule(testBroadcastTask);
    }
    
    
    /**
     * Function that prints the message to text area. If the message is an error
     * then writes it to specific output
     *
     * @param message String containing the message
     * @param error Boolean that indicates an error message
     */
    private void printStat(String message, boolean error) {
        status.append(message + "\n");
        if (error) {
            System.err.println(message);
            status.append("****Stoping host app****");
        }

    }


    /**
     * Start up the host application.
     *
     * @param args any command line arguments
     * @throws java.lang.Exception error
     */
   
    
    
    public static void main(String[] args) throws Exception {
        // TODO
        // register the application's name with the OTA Command server & start OTA running
     //   OTACommandServer.start("SunApp-Host-Test");

        TestSunApp app = new TestSunApp();

        app.startApp();
        app.testAPI(DataManagement.HTTPGET);
        
        app.testBroadcastTask();
        
        //Api test code:
    }

}

