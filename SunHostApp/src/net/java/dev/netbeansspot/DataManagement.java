package net.java.dev.netbeansspot;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextArea;

/**
 * <h1>Data Management class</h1>
 * This class is used to manage the api connection and data transmision in both
 * directions.
 *
 * @author Ovidiu Mircea Moldovan
 * @author Juan Perez
 * @version 1.0
 * @since 2015-12-14
 */
public class DataManagement {

    //POST request status values
    private static final int INACTIVE = 0;
    private static final int CONNECTING = 1;
    private static final int COMPLETED = 2;
    private static final int IOERROR = 3;
    private static final int PROTOCOLERROR = 4;

    private static int POST_STATUS = INACTIVE;

    /**
     * HTTP GET request
     */
    public static final String HTTPGET = "GET";

    /**
     * HTTP POST request
     */
    public static final String HTTPPOST = "POST";

    private static JTextArea statusTextArea;

    /**
     * Constructor
     *
     * @param stat JTextArea oject where the messages are displayed
     */
    public DataManagement(JTextArea stat) {
        //asign a JTextArea to write to
        statusTextArea = stat;
    }

    /**
     * HTTP GET request
     *
     * @param api_url URL request object
     * @param proxy Sets a proxy if is needed
     * @return int Response code value
     * @throws Exception error
     */
    public int sendGet(URL api_url, Proxy proxy) throws Exception {
        URL obj = api_url;

        //HttpURLConnection con = (HttpURLConnection) obj.openConnection(proxy);
        //sin proxy
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod(DataManagement.HTTPGET);

        int responseCode = con.getResponseCode();
        printStat("\nSending 'GET' request to URL : " + api_url, false);
        printStat("Response Code : " + responseCode, false);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        printStat(response.toString(), false);
        return Integer.valueOf(response.toString());
    }

    /**
     * Intermediate method for command request. A posh way to call sendPost
     * method.
     *
     * @param postUrl the post request URL
     * @param postUrlParameters the post request parameters
     * @param proxy used for ETSIST intrared
     * @return the post request response
     */
    public String getCommand(URL postUrl, String postUrlParameters, Proxy proxy) {
        String return_val = "";

        try {
            return_val = sendPost(postUrl, postUrlParameters, proxy);
        } catch (Exception ex) {
            printStat(ex.toString(),true);
            Logger.getLogger(DataManagement.class.getName()).log(Level.SEVERE, null, ex);
        }
        return return_val;

    }

    /**
     * HTTP POST request.
     *
     * @param api_url URL object for base url
     * @param urlParams String containing data
     * @param proxy Proxy object
     * @return the response
     * @throws Exception error
     */
    // 
    public String sendPost(URL api_url, String urlParams, Proxy proxy) throws Exception {

        URL obj = api_url;
        //HttpURLConnection con = (HttpURLConnection) obj.openConnection(proxy);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        //add reuqest header
        con.setRequestMethod(DataManagement.HTTPPOST);

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParams);
        wr.flush();
        wr.close();

        // Rread post request
        int responseCode = con.getResponseCode();
        printStat("\nSending 'POST' request to URL : " + obj.toString(), false);
        printStat("Post parameters : " + urlParams, false);
        printStat("Response Code : " + responseCode, false);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        printStat(response.toString(), false);
        return response.toString();
       
    }

    /**
     * Function that prints the message to text area. If the message is an error
     * then writes it to specific output
     *
     * @param message String containing the message
     * @param error Boolean that indicates an error message
     */
    private static void printStat(String message, boolean error) {
        statusTextArea.append(message + "\n");

        if (error) {
            System.err.println(message);

        } 

    }

}
