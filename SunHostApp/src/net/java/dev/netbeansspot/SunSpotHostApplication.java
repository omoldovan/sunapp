package net.java.dev.netbeansspot;

import com.sun.spot.io.j2me.radiogram.*;
import com.sun.spot.peripheral.ota.OTACommandServer;
import com.sun.spot.util.IEEEAddress;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.microedition.io.*;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * <h1>Host App</h1>
 * This application is the host module of SunApp. This host application collects
 * sensor samples sent by the 'on SPOT' portion running on neighboring SPOTs and
 * graphs them in a window.
 *
 * @author Ovidiu Mircea Moldovan
 * @author Juan Perez
 * @version 1.0
 * @since 2015-12-14
 */
public class SunSpotHostApplication {

    // Broadcast port on which we listen for sensor samples
    private static final int HOST_PORT = 69;
    // Broadcast port on which we send alarms and commands
    private static final int AGREGATOR_PORT = 59;
    // Text area where the samples are displayed
    private JTextArea status;
    private DataManagement send;
    private String urlParameters;
    private URL api_url;
    private String getUrl = "http://api.thingspeak.com/update?";
    private static final Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("proxylabdiatel.euitt.upm.es",
            3128));
    private String postUrl = "https://api.thingspeak.com/talkbacks/4532/commands/execute.json";
    private String postUrlParameters = "api_key=LXYMJZ9ZRCXHS16H";

    //communication to the agregator is performed inside this thread
    private Thread broadcastTask;
    private RadiogramConnection rConGet;

    private RadiogramConnection rConSend;

    //ALARMS
    private static final String ALARMTEMP = "ALARMA DE TEMPERATURA";
    private static final String ALARMLUM = "ALARMA DE LUMINOSIDAD";
    private static final String ABRIRPUERTA = "ABRIR PUERTA";
    private static final String CERRARPUERTA = "CIERRA PUERTA";
    private static final String ALUMNOALERTA = "ALARMA DE ALUMNO";

    /**
     * Host mandatory method. Starts the host. Creates a JFrame where the status
     * will be displayed
     *
     */
    private void setup() {
        JFrame fr = new JFrame("Send Data Host App");
        status = new JTextArea();
        JScrollPane sp = new JScrollPane(status);
        fr.add(sp);
        fr.setSize(360, 200);
        fr.validate();
        fr.setVisible(true);
        printStat("****Starting host app****", false);
        //initialize the data management object
        send = new DataManagement(status);

        try {
            //initialize the main connection channels and URLs
            api_url = new URL("http://api.thingspeak.com/update.json");
            
            rConSend = (RadiogramConnection) Connector.open("radiogram://broadcast:" + AGREGATOR_PORT);

            broadcastTask();
        } catch (MalformedURLException ex) {
            printStat("Error " + Level.SEVERE + "  " + ex, true);
            Logger.getLogger(SunSpotHostApplication.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SunSpotHostApplication.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Host mandatory method. Holds the program logic.
     *
     * @throws Exception
     */
    private void run() throws Exception {

        Radiogram dg;
       while (true){
        try {
        
           
            // Open up a server-side broadcast radiogram connection
            // to listen for sensor readings being sent by different SPOTs
            rConGet = (RadiogramConnection) Connector.open("radiogram://:" + HOST_PORT);

            dg = (Radiogram) rConGet.newDatagram(rConGet.getMaximumLength());
           
            String ieee = IEEEAddress.toDottedHex(dg.getAddressAsLong());
            printStat("Received packet from SPOT: " + ieee, false);

            printStat("Listening...", false);

            // Main data collection loop
                // Read  sensor sample received over the radio and send it
                rConGet.receive(dg);
                String msg_fromAgregator = dg.readUTF();
                printStat(msg_fromAgregator, false);
                String fields[] = msg_fromAgregator.split(" ");

                urlParameters = "api_key=IXTF61FOCJSLC3CX&field1="
                        + fields[1] + "&field2=" + fields[4];
                printStat(urlParameters, false);
                api_url = new URL(getUrl + urlParameters);

                send.sendGet(api_url, proxy);

                rConGet.close();
                Thread.sleep(1000);

            }
         catch (Exception e) {
            printStat("Caught " + e + " while reading sensor samples.", true);
            throw e;
        }
}
    }

    /**Method that broadcast the data parsed from the command.
     * 
     * 
     * @param rCon
     * @param dataG
     * @return
     */
    private boolean broadcastDG(String dataG) {
        try {
            
            //creating a new datagram for each command
            Datagram dg = rConSend.newDatagram(rConSend.getMaximumLength());  // 
            dg.writeUTF(dataG);
            
            //send the message
            rConSend.send(dg);
            
            System.out.println("Mensaje enviado: " + dataG);
            
            return true;
        } catch (IOException ex) {
            Logger.getLogger(SunSpotHostApplication.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    /**Method that parse the command recieved from thingspeak.
     * There are only 4 valid values, any other will return an empty string.
     *
     * @param command
     * @return
     */
    private String process_command(String command) {

        //if message within this values
        if (command.contains(ALARMTEMP)) {
            return ALARMTEMP;
        } else if (command.contains(ALARMLUM)) {
            return ALARMLUM;
        } else if (command.contains(ABRIRPUERTA)) {
            return ABRIRPUERTA;
        } else if (command.contains(CERRARPUERTA)) {
            return CERRARPUERTA;
        } else if (command.contains(ALUMNOALERTA)) {
            return ALUMNOALERTA;
        }
        //else no valid command reconized
            else {
            printStat("El commando no se reconoce", true);
        }

        return "";

    }

    /**
     * This method creates and starts a new thread to monitor and execute remote
     * commands (thingspeak). Uses the API to request for new commands. Once it
     * has one parse it and boradcast the parse data to the sensor network
     * (agregator).
     *
     */
    public void broadcastTask() {

        broadcastTask = new Thread(new Runnable() {

            /**
             * thread main function
             *
             */
            public void run() {
        
                            while (true) {
        
                   try {

                    String command;

                    //repeat this actions forever
                        //check for new commands
                        command = send.getCommand(new URL(postUrl), postUrlParameters, proxy);

                        printStat("Commando: " + command, false);
                        
                        //if there is a new command...
                        if (command.compareTo("{}") != 0) {

                            String a_message = process_command(command);
                            printStat("Just recieved the command/alarm: " + a_message, false);
                
                            //if a valid command broadcast it's parsed string
                            if (!a_message.isEmpty() && !broadcastDG(a_message)) {
                                printStat("Error during command broadcast", true);
                            }
                        }
                        Thread.sleep(100);
                   

                } catch (Exception ex) {
                    ex.printStackTrace();
                    printStat("Error during command reading ", true);
                }
                }
            }
        });
        //start thread
        broadcastTask.start();
    }

    /**
     * This method stops the thread started by this class.
     */
    public void broadcastStop() {
        broadcastTask.stop();

    }

    /**
     * Function that prints the message to text area. If the message is an error
     * then writes it to specific output
     *
     * @param message String containing the message
     * @param error Boolean that indicates an error message
     */
    private void printStat(String message, boolean error) {
        status.append(message + "\n");
        if (error) {
            System.err.println(message);
            status.append("****Stoping host app****");
        }
    }

    /**
     * Start up the host application.
     *
     * @param args any command line arguments
     * @throws java.lang.Exception error
     */
    public static void main(String[] args) throws Exception {

        // register the application's name with the OTA Command server & start OTA running
        OTACommandServer.start("SunApp-Host");

        SunSpotHostApplication app = new SunSpotHostApplication();
        app.setup();
        app.run();

    }

}
