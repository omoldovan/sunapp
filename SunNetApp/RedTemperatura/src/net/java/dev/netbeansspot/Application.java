package net.java.dev.netbeansspot;

import com.sun.spot.io.j2me.radiogram.RadiogramConnection;
import com.sun.spot.resources.Resources;
import com.sun.spot.resources.transducers.ITemperatureInput;
import com.sun.spot.util.Utils;
import javax.microedition.io.Connector;
import javax.microedition.io.Datagram;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

/**
 * <h1>Aplicacion Temperatura</h1>
 *
 * @author Juan Perez/Ovidiu Moldovan
 * @author Juan Perez/Ovidiu Moldovan
 * @version 1.0
 * @since 2015-12-14
 * 
* Esta Aplicacion se Encarga de Medir la temperatura en una mota y enviar la
 * informacion a la Mota-Agragador cada 5 segundos.
 * 
*/
public class Application extends MIDlet {

    /**
     * Aqui definimos la instancia del sensor que vamos a utilizar y las
     * constantates que necesitamos para determinar el tiempo de sampleo y el
     * puerto que se usara para transmitir las medidas al Nodo Agregador.
     */
    private final ITemperatureInput sensorTemperatura = (ITemperatureInput) Resources.lookup(ITemperatureInput.class);
    private static final int HOST_PORT = 66;
    private static final int SAMPLE_PERIOD = 5 * 1000;  // in milliseconds

    /**
     * Función que se ejecuta al arranque del programa. Ejecuta toda la lógica
     * del programa.
     *
     * @throws MIDletStateChangeException
     */
    protected void startApp() throws MIDletStateChangeException {
        /**
         * Se imprime por pantalla la direccion asignada a la mota que mide la
         * temperatura, esto sirve para debugear que la aplicacion arranco
         * correctamente.
         */
        String ourAddress = System.getProperty("IEEE_ADDRESS");
        System.out.println("Starting temp application on " + ourAddress + " ...");
        /**
         * Se inicializa un contador para conocer la cantidad de medidas que
         * realiza el sensor durante el ciclo while.
         */
        long contador = 0;
        while (true) {
            try {
                /**
                 * Se abre el puerto de Broadcast con el valor definido en las
                 * constantes al inicio de la clase, adicionalmente se crea un
                 * objeto de la clase Datagrama el cual sera utilizado para
                 * emviar y recibir datos.
                 */
                RadiogramConnection rCon = (RadiogramConnection) Connector.open("radiogram://broadcast:" + HOST_PORT);
                Datagram dg = rCon.newDatagram(rCon.getMaximumLength());  // 
                /**
                 * Se hace la lectura de la temperatura en la mota haciendo uso
                 * de uno de los metodos definidos para los recursos de la
                 * Interfaz ITemperatureInput, este valor se imprime por
                 * pantalla junto con el contador de las mediciones realizadas.
                 */
                double tempInstantanea = sensorTemperatura.getCelsius();
                System.out.println("Mensaje temp numero= " + contador);
                System.out.println("Temperatura actual es: " + tempInstantanea);

                /**
                 * La informacion obtenida del sensor se empaqueta en un
                 * datagrama.
                 */
                dg.writeUTF("temp/" + tempInstantanea);
                /**
                 * se envia por bradcast al puerto que estara escuchando el nodo
                 * agregador.
                 */
                rCon.send(dg);
                /**
                 * Se debe cerrar la conexion, ya que al inicio de cada ciclo se
                 * establece una nueva conexion.
                 */
                rCon.close();
                /**
                 * Se coloca un salto de linea en la consola para visualizar
                 * mejor la información y tener una instruccion que permita
                 * verifica que no hubo problemas con la ejecucion de las
                 * intrucciones del broadcast.
                 */
                System.out.println("\n");
                /**
                 * Se incrementa el contador antes de realizar la pausa de 5
                 * segundos al final del ciclo.
                 */
                contador++;
                /**
                 * se realiza la pausa utilizando la constante definida al
                 * inicio de la clase.
                 */

                Utils.sleep(SAMPLE_PERIOD);

            } catch (Exception e) {
                /**
                 * Se coloca un catch para manejar alguna posible excepcion y se
                 * imprime por consola un mensaje que permita debuguear en que
                 * lugar ocurrio el problema.
                 */
                System.err.println("Caught " + e + " while collecting/sending sensor sample.");
            }
        }
    }

    /**
     * No se usa.
     *
     */
    protected void pauseApp() {
        // This is not currently called by the Squawk VM
    }

    /**
     * Función propia de la plataforma que se ejecuta al cerrar el programa.
     *
     * @param unconditional
     * @throws MIDletStateChangeException
     */
    protected void destroyApp(boolean unconditional) throws MIDletStateChangeException {
        /**
         * se imprime un mensaje de salida de la aplicación.
         */
        System.out.println("****Apagando....*********");
    }
}
