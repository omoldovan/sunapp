package net.java.dev.netbeansspot;

import com.sun.spot.io.j2me.radiogram.Radiogram;
import com.sun.spot.io.j2me.radiogram.RadiogramConnection;
import com.sun.spot.util.IEEEAddress;
//import com.sun.spot.util.Utils;
import javax.microedition.io.Connector;
import javax.microedition.io.Datagram;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;
import com.sun.spot.io.j2me.radiogram.*;
import com.sun.spot.peripheral.ota.OTACommandServer;
import com.sun.spot.resources.Resources;
import com.sun.spot.resources.transducers.ITriColorLEDArray;
import com.sun.spot.resources.transducers.LEDColor;

import com.sun.spot.util.IEEEAddress;
import java.io.IOException;

/**
 * <h1>Aplicacion Agregador</h1>
 *
 * @author Juan Perez
 * @author Ovidiu Moldovan
 * @version 1.0
 * @since 2015-12-14
 * 
* Esta Aplicacion se encarga de recibir la informacion que envian las motas de
 * temperatura y de luz y armar un unico mensaje que se envia a la
 * Mota-Sumidero(Sink).
 * 
*/
public class Application extends MIDlet {

    /**
     * Aqui definimos las constantates que necesitamos para indicar el puerto
     * que se usara el Nodo Agregador para recibir los mensajes de las motas y
     * el puerto al cual hara broadcast (debe ser el mismo euq esta ¨escuchando¨
     * el Nodo Sumidero.
     */
    //Para recepcion desde las Motas.
    private static final int HOST_PORT_SPOTS = 66;
    //PARA RECEPCION DESDE EL SINK
    private static final int HOST_PORT = 59;
    //PARA ENVIO AL SINK
    private static final int HOST_PORT_SINK = 69;
    /**
     * Inicializamos los Strings que se usaran para manejar la informacion
     * entrante y saliente del nodo agregador.
     */
    private String tempValue = "";
    private String luzValue = "";
    private String msg_toSink = "";
    private boolean sent = false;
    private Thread escucharTask;
    
    /**
     * Estos seran los posibles mensajes que recibire del SINK
     */
    private static final String ALARMTEMP="ALARMA DE TEMPERATURA";
    private static final String ALARMLUM="ALARMA DE LUMINOSIDAD";
    private static final String ABRIRPUERTA="ABRIR PUERTA";
    private static final String CERRARPUERTA="CIERRA PUERTA";
    private static final String ALUMNOALERTA = "ALARMA DE ALUMNO";
    
    /**
     * Inicializo la Interfaz para controlar los leds del Nodo Agregador
     */
    private final ITriColorLEDArray leds = (ITriColorLEDArray)Resources.lookup(ITriColorLEDArray.class);
    private final LEDColor[] colors = { LEDColor.RED, LEDColor.GREEN, LEDColor.BLUE };
    private  RadiogramConnection rCon;
               
    /**
     * Función que se ejecuta al arranque del programa. Ejecuta toda la lógica
     * del programa.
     *
     * @throws MIDletStateChangeException
     */
    protected void startApp() throws MIDletStateChangeException {
        /**
         * Se imprime por pantalla la direccion asignada a la mota agregadora,
         * esto sirve para debugear que la aplicacion arranco correctamente.
         */
        String ourAddress = System.getProperty("IEEE_ADDRESS");
        System.out.println("Iniciando aplicacion Agregador en nodo: " + ourAddress + " ...");
           try{
                
                    String msg_fromHost = "";
                    // Open up a server-side broadcast radiogram connection
                    // to listen for sensor readings being sent by ThingSpeak through the SINKA
                    rCon = (RadiogramConnection) Connector.open("radiogram://:" + HOST_PORT);
        escucharTask();
           }catch (Exception e) {
                    System.err.println("ERROR opening connection" + e.getMessage());
                }
            
      
        while (true) {
            try {
                /**
                 * Abrimos una conexion tipo servidor para recibir los
                 * datagramas enviados como Broadcast por los Nodos medidores,
                 * ambos envian sus mensajes por el mismo puerto
                 */
                RadiogramConnection rCon_fromSpots = (RadiogramConnection) Connector.open("radiogram://:" + HOST_PORT_SPOTS);
                Datagram dg_fromSpots = rCon_fromSpots.newDatagram(rCon_fromSpots.getMaximumLength());

                /**
                 * Extraigo los mensajes que llegan por la conexion con las
                 * motas
                 */
                rCon_fromSpots.receive(dg_fromSpots);
                String msg_fromSpot = dg_fromSpots.readUTF();
                /**
                 * verifico cual es la cabecera del mensaje para identificar de
                 * que mota viene el mensaje, y dependiendo de donde venga se lo
                 * asigno a una variable u otra.
                 */
                if (msg_fromSpot.startsWith("temp")) {
                    tempValue = msg_fromSpot.substring(5);
                }

                if (msg_fromSpot.startsWith("luz")) {
                    luzValue = msg_fromSpot.substring(4);
                }
                /**
                 * Cierro el puerto con las motas sensoras ya que inicio la
                 * comunicacion al inicio del ciclo.
                 */
                rCon_fromSpots.close();
                //Logica para formar el mensaje
                if ((tempValue != "") && (luzValue != "")) {
                    /**
                     * Aqui armo el mensaje que voy enviar al nodo sumidero,
                     * donde ensamblo la informacion recibida por ambos nodos
                     * sensores, adicionalmente imprimo el mensaje por consola
                     * para validar el formato que le damos al mensaje.
                     */
                    msg_toSink = "Temp: " + tempValue + " - Luz: " + luzValue;
                    System.out.println(msg_toSink);

                    /**
                     * Inicio una conexion con el nodo sumidero e inicializo el
                     * datagrama que utilizare para enviar la informacion.
                     */
                    RadiogramConnection rCon_toSink = (RadiogramConnection) Connector.open("radiogram://broadcast:" + HOST_PORT_SINK);
                    Datagram dg_toSink = rCon_toSink.newDatagram(rCon_toSink.getMaximumLength());  //
                    /**
                     * Asigno el paquete que ensamble al datagrama y lo envio al
                     * nodo sumidero, finalmente cierro la conexion para que no
                     * haya errores cuando se repita el ciclo.
                     */
                    dg_toSink.writeUTF(msg_toSink);
                    rCon_toSink.send(dg_toSink);
                    rCon_toSink.close();
                    /**
                     * Antes de terminar imprimo por pantalla un mensaje para
                     * poder verificar que el envio se ejecuto efectivamente.
                     */

                    System.out.println("Mensaje to Sink = Enviado");
                    sent = true;
                    
                    
                 Float luzV = new Float(Float.parseFloat(luzValue));
                 Float tempV= new Float(Float.parseFloat(tempValue));
                 
                    if(luzV.intValue() < 600){
                        leds.getLED(1).setOff();
                    }
                    if(tempV.intValue() < 35.00 ) {
                            leds.getLED(0).setOff();
                        }
                }

                resetValues();

            } catch (Exception e) {
                /**
                 * Se coloca un catch para manejar alguna posible excepcion y se
                 * imprime por consola un mensaje que permita debuguear en que
                 * lugar ocurrio el problema.
                 */
                System.err.println("Caught " + e + " while collecting/sending sensor sample.");
            }

        }
    }
    
    
    private void resetValues() {
        if (sent) {
            tempValue = "";
            luzValue = "";
            sent = false;
        }

    }

    /**Thread que escucha los mensajes del host.
     * Los mensajes tiene el formato 
     * private static final String ALARMTEMP="ALARMA DE TEMPERATURA";
    private static final String ALARMLUM="ALARMA DE LUMINOSIDAD";
    private static final String ABRIRPUERTA="ABRIR PUERTA";
    private static final String CERRARPUERTA="CERRAR PUERTA";
     */
    public void escucharTask() {

        escucharTask = new Thread(new Runnable() {

            public void run() {
              
                //defino led 0 rojo para las alarmas de temperatura
                leds.getLED(0).setRGB(255, 0, 0);
                //defino led 1 blanco para las alarmas de luz
                leds.getLED(1).setRGB(255, 255, 255);
                //defino led 2 naranja para las alarmas de puerta cerrada
                leds.getLED(2).setRGB(230, 95, 0);
                leds.getLED(2).setOn();
                //defino led 3 verde para las alarmas de puerta abierta
                leds.getLED(3).setRGB(0, 255, 0);
                // defino led 4 para alarma del punto 8 (opcional)
                leds.getLED(4).setRGB(0,0,255);
                Radiogram dg;
            
                String msg_fromHost = "";
                while (true){
                 
                try {
                     
                   
                   
                    dg = (Radiogram) rCon.newDatagram(rCon.getMaximumLength());
                   
                    String ieee = IEEEAddress.toDottedHex(dg.getAddressAsLong());
                    System.out.println("Received packet from SPOT: " + ieee);

                    System.out.println("Listening...");

                    // Main data collection loop
                    // Read  message received
                    rCon.receive(dg);
                    msg_fromHost = dg.readUTF();
                //********************************************
                    //TODO:  manejar el mensaje que te llega
                    //Práctica 3: punto 7) 
                    System.out.println(msg_fromHost);
                 
                    
                 ledsManagement(msg_fromHost);   
                    
                        
                //********************************************    
               //     rCon.close();
                    Thread.sleep(1000);
                 
                }     
                catch (Exception e) {
                    System.err.println("setUp caught " + e.getMessage());
                    System.out.println("Error during task of acceleartion read"+e.toString());
                }
                }
            }
           
        });

        escucharTask.start();

    }

    
    
    /**Method that parse the command recieved from thingspeak.
     * There are only 4 valid values, any other will return an empty string.
     *
     * @param command
     * @return
     */
    private void ledsManagement(String command) {
try{
        String msg_fromHost=command;
                    
                    if(msg_fromHost.equals(ALARMTEMP)){
                        //Enciendo Led 0 (ROJO) mientras que tempValue > 35
                        leds.getLED(0).setOn();
                 
                    }
                    else if(msg_fromHost.equals(ALARMLUM)){
                        //Enciendo Led 1 (Blanco) mientras que luzValue > 600
                        leds.getLED(1).setOn();
                    
                    }
                    
                    else if(msg_fromHost.equals(CERRARPUERTA)){
                        //Enciendo Led 2 (Naranja)
                        leds.getLED(2).setOn();
                        leds.getLED(3).setOff();
                    }
                    else if(msg_fromHost.equals(ABRIRPUERTA)){
                        //Enciendo Led 2 (Verde)
                        leds.getLED(2).setOff();
                        leds.getLED(3).setOn();
                    }
                    else if(msg_fromHost.equals(ALUMNOALERTA)){
                        //Enciendo Led 4 (aZUL)
                        leds.getLED(4).setOn();
                        Thread.sleep(2000);
                        leds.getLED(4).setOff();
                    }
                    
        //else no valid command reconized
            else {
                    System.err.println("El commando no se reconoce");
        }
}catch (Exception ex){
System.err.println("Error: "+ex);
       
}
    }
    
    /**
     * No se usa.
     *
     */
    protected void pauseApp() {
        // This is not currently called by the Squawk VM
    }

    /**
     * Función propia de la plataforma que se ejecuta al cerrar el programa.
     *
     * @param unconditional
     * @throws MIDletStateChangeException
     */
    protected void destroyApp(boolean unconditional) throws MIDletStateChangeException {
        /**
         * se imprime un mensaje de salida de la aplicación.
         */
        System.out.println("****Apagando....*********");
    }
}
